class CreatePages < ActiveRecord::Migration
  def up
    create_table :pages do |t|
    	t.references :subject, index: true, foreign_key: true

    	t.string "name", :null => false, :limit => 40
    	t.string "permalink", index: true
    	t.integer "position"
    	t.boolean "visible", :default => true
      	t.timestamps null: false
    end
  end

  def down
  	drop_table :pages
  end

end
