class AdminUser < ActiveRecord::Base

	# to configure a different table name;
	# self.table_name = "admin_users"

	has_secure_password

	# ActiveRecord Relations
	has_and_belongs_to_many :pages, :join_table => "admin_users_pages"
	has_many :section_edits
	has_many :sections, :through => :section_edits

	# Validation Rules
	EMAIL_REGEX = /\A[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}\Z/i
	FORBIDDEN_USERNAMES = ['littlebopeep', 'humptydumpty', 'marymary']

	# shortcut validations, aka "sexy validations"
	validates :first_name, :presence => true, :length => {:maximum => 25}
	validates :last_name, :presence => true, :length => {:maximum => 50}
	validates :username, :presence => true, :length => {:within => 8..25}, 
		:uniqueness => true
	validates :email, :presence => true, :length => {:maximum => 100}, 
		:format => EMAIL_REGEX, :confirmation => true

	validate :username_is_allowed
	# validate :no_new_users_on_saturday, :on => :create

	# Custom Validation
	def username_is_allowed
		if FORBIDDEN_USERNAMES.include?(username)
			errors.add(:username, "has been restricted from use.")
		end
	end

	# Errors not related to a specific attribute can be added to errors[:base]
	def no_new_users_on_saturday
		if Time.now.wday == 6
			errors[:base] << "No new users on Saturdays."
		end
	end

	# Named Scopes
	scope :sorted, lambda {order("admin_users.last_name ASC, admin_users.first_name ASC")}

	# Custom Methods
	def name
		return "#{first_name} #{last_name}"
	end

end
