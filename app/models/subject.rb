class Subject < ActiveRecord::Base
	
	# Realtionships
	# Could delete related pages automatically
	# whenever a subject is deleted:
	# has_many :pages, :dependent => :destroy
	has_many :pages

	acts_as_list

	# Validations
	# Don't need to validate (in most cases) :
	# ids, foreign keys, timestamps, boolean, counters
	validates_presence_of :name
	validates_length_of :name, :maximum => 255

	# validates_presence_of vs. validates_length_of :minimum => 1
	# different error messages: "can't be blank" or "is too short"
	# validates_length_of allows string with only spaces!

	# Named Scopes 
	scope :visible, lambda {where(:visible => true)}
	scope :invisible, lambda {where(:visible => false)}
	scope :sorted, lambda{order("subjects.position ASC")}
	scope :newest_first, lambda{order("subjects.created_at DESC")}
	scope :search, lambda{|query|
		where(["name LIKE ?", "%#{query}%"])
	}

end
